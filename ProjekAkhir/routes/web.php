<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\JenisController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/', [UserController::class,'index']);

Route::get('/biodata', [HomeController::class, 'bio']);

Route::post('/kirim', [HomeController::class, 'kirim']);

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

//CRUD
//Create Data
//Route mengarah ke form tambah kategori
Route::get('/Kategori/create', [KategoriController::class, 'create']);
//Route untuk menyimpan inputan kedalam database table kategori
Route::post('/Kategori', [KategoriController::class, 'store']);

//Read
//Route mengarah ke halaman tampil semua data di table kategori
Route::get('/Kategori', [KategoriController::class, 'index']);

//Update Data
//Route mengarah form edit kategori
Route::get('/Kategori/{id}/edit', [KategoriController::class, 'edit']);
//Route mengarah form edit data berdasarkan id kategori
Route::put('/Kategori/{id}', [KategoriController::class, 'update']);

//Delete Data
Route::delete('/Kategori/{id}', [KategoriController::class, 'destroy']);

////////////////////////////////////////////////////////////////////////////////

//CRUD Jenis
//Create Data

//menampilkan halaman tambah jenis
Route::get('/Jenis/create', [JenisController::class,'create']);
//menyompan inputan kedalam database jenis
Route::post('/Jenis',[JenisController::class,'store']);
//menampilkan halaman jenis
Route::get('/Jenis', [JenisController::class,'index']);

//menampilkan halaman edit
Route::get('/Jenis/{id}/edit', [JenisController::class,'edit']);

/////////////////////////////////////////////////////////////////////

//CRUD user
//Create Data

//menampilkan halaman tambah user
Route::get('/User/create', [UserController::class,'create']);

//menampilkan halaman users
Route::get('/User', [UserController::class,'index']);

//menampilkan halaman edit
Route::get('/User/{id}/edit', [UserController::class,'edit']);
