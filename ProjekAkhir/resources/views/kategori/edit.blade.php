@extends('layouts.master')
@section('title')
    Halaman Edit Kategori
@endsection

@section('sub-title')
    Kategori
@endsection

@section('content')

<form action="/Kategori/{{$tbkategori->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name= "nama" value="{{$tbkategori->nama}}" class="form-control" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection