@extends('layouts.master')
@section('title')
    Halaman Tampil Kategori
@endsection

@section('sub-title')
    Kategori
@endsection
@section('content')

<a href="/Kategori/create" class="btn btn-primary btn-sm my-2">Tambah  Kategori</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Aksi</th>

      </tr>
    </thead>
        <tbody>
            @forelse ($tbkategori as $key=> $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        
                        <form action="/Kategori/{{$item->id}}"method="POST">
                            @csrf
                            @method('delete')
                            
                        <a href="/Kategori/{{$item->id}}/edit"class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
            <tr>
                <td>Data Kategori Kosong</td>
            </tr>
            @endforelse
        </tbody>
  </table>

@endsection