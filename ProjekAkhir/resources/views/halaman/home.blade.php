@extends('layouts.master')
@section('title')
    Halaman Welcome
@endsection

@section('sub-title')
    Welcome
@endsection

@section('content')
    <h1>Selamat Datang {{$nama}} {{$alamat}}</h1>
    <p>Jenis Kelamin =
        @if($jenisKelamin === "1")
            Laki- Laki
        @else
            Perempuan
        @endif
 @endsection       
