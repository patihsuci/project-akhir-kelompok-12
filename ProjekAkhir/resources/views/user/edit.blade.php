@extends('layouts.master')
@section('title')
    Halaman Tambah Jenis
    Halaman Edit User
@endsection

@section('sub-title')
    Jenis
@endsection

@section('content')
    <form action="" method="post">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Password</label>
            <input type="text" name="password" class="form-control">
        </div>
        @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Hak Akses</label>
            <div class="dropdown" name="hak_akses">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    --pilih Hak Akses--
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" name="ketegori">
                    <a class="dropdown-item" value="admin">Admin</a>
                    <a class="dropdown-item" value="karyawan">Karyawan</a>
                </div>
            </div>
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
