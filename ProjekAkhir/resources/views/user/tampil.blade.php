@extends('layouts.master')
@section('title')
    Halaman Tampil User
@endsection

@section('sub-title')
    User 
@endsection
@section('content')

<a href="/User/create" class="btn btn-primary btn-sm my-2">Tambah  User</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Hak Akses</th>
        <th scope="col">Password</th>
        <th scope="col">Aksi</th>

      </tr>
    </thead>
        <tbody>
            @forelse ($user as $key=> $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->hak_akses}}</td>
                    <td>{{$item->password}}</td>
                </tr>
            @empty
            <tr>
                <td>Data Kategori Kosong</td>
            </tr>
            @endforelse
        </tbody>
  </table>

@endsection