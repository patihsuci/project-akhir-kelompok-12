@extends('layouts.master')
@section('title')
    Halaman Tampil Kategori
@endsection

@section('sub-title')
    Kategori 
@endsection
@section('content')

<a href="/Jenis/create" class="btn btn-primary btn-sm my-2">Tambah  Jenis</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Gambar</th> 
        <th scope="col">merek</th>
        <th scope="col">jumlah</th>
        <th scope="col">Aksi</th>

      </tr>
    </thead>
        <tbody>
            @forelse ($jenis as $key=> $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td><img src="{{Url('./file/'.$item->gambar)}}" class="image-responsive "style='width:120px;height:130px'></td>
                    <td>{{$item->merek}}</td>
                    <td>{{$item->jumlah}}</td>
                    <td>
                        
                        <form action="/Jenis/{{$item->id}}"method="POST">
                            @csrf
                        <a href="/Jenis/{{$item->id}}/edit"class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
            <tr>
                <td>Data Kategori Kosong</td>
            </tr>
            @endforelse
        </tbody>
  </table>

@endsection