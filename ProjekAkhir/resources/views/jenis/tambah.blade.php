@extends('layouts.master')
@section('title')
    Halaman Tambah Jenis
@endsection

@section('sub-title')
    Jenis
@endsection

@section('content')
    <form action="/Jenis" method="POST" enctype="multipart/form-data">
        @method('POST')
        @csrf
        <div class="form-group">
            <label>Merek</label>
            <input type="text" name="merek" class="form-control">
        </div>
        @error('merek')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Gambar</label>
            <input type="file" name="gambar" class="form-control">
        </div> 

        <div class="form-group">
            <label>Jumlah</label>
            <input type="text" name="jumlah" class="form-control">
        </div>
        @error('jumlah')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Kategori</label>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    --pilih kategori--
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" name="ketegori">
                    @forelse ($kategori as $key=>$item)
                        <a class="dropdown-item" value="{{ $item->id }}">{{ $item->nama }}</a>
                    @empty
                        <a class="dropdown-item">kosong</a>
                    @endforelse
                </div>

            </div>
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
