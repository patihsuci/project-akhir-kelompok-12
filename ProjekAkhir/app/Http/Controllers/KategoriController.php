<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',

        ],
        [
            'nama.required'=>"Nama Kategori tidak boleh kosong!"
        ]
        ); 

        DB::table('tbkategori')->insert([
            'nama' => $request ['nama']
            
        ]);
        return redirect('/Kategori');
    }

    public function index()
    {
        $tbkategori = DB::table('tbkategori')->get();

        return view('Kategori.tampil', ['tbkategori' => $tbkategori]);
    }
    public function edit($id)
    {
        $tbkategori = DB::table('tbkategori')->find($id);

        return view('Kategori.edit', ['tbkategori' => $tbkategori]);
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',

        ],
        [
            'nama.required'=>"Nama Kategori tidak boleh kosong!"
        ]
        ); 
        DB::table('tbkategori')
              ->where('id', $id)
              ->update(
                ['nama' => $request['nama']
                ]
            );

        return redirect('/Kategori');
    }
    public function destroy($id)
    {
        DB::table('tbkategori')->where('id', '=', $id)->delete();

        return redirect('/Kategori');
    }

}
