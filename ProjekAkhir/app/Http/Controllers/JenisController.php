<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class JenisController extends Controller
{

    function __construct(Request $request)
    {

        $this->request = $request;
    }
    public function create()
    {
        $kategori = DB::table('tbkategori')->get();
        return view('jenis.tambah', ['kategori' => $kategori]);
    }

    public function store(Request $request)
    {
        $ext = $this->request->file('gambar');
        $tgl = date('Y-m-d');
        if ($ext == '') {
        } else {
            $setname = rand(122, 333) . '-' . $tgl . '.' . $ext->getClientOriginalExtension();
            $ext->move('./file/', $setname);
        }
        $request->validate(
            [
                'merek' => 'required',
                'jumlah' => 'required',


            ],
            [
                'merek.required' => "Merek Kategori tidak boleh kosong!",
                'jumlah.required' => "Jumlah tidak boleh kosong!",

            ]
        );
        DB::table('tbjenis')->insert([
            'merek' => $request['merek'],
            'gambar' =>  $setname,
            'brand' =>  $request['merek'],
            'tbkategori_id' => 1,
            'jumlah' => $request['jumlah'],

        ]);
        return redirect('/Jenis');
    }

    public function update(Request $request, $id)
    {
        $ext = $this->request->file('gambar');
        $tgl = date('Y-m-d');
        if ($ext == '') {
        } else {
            $setname = rand(122, 333) . '-' . $tgl . '.' . $ext->getClientOriginalExtension();
            $ext->move('./file/', $setname);
        }
        $request->validate(
            [
                'merek' => 'required',
                'jumlah' => 'required',


            ],
            [
                'merek.required' => "Merek Kategori tidak boleh kosong!",
                'jumlah.required' => "Jumlah tidak boleh kosong!",

            ]
        );
        DB::table('tbjenis')->wheree(['id' => $id])->update([
            'merek' => $request['merek'],
            'gambar' =>  $setname,
            'brand' =>  $request['merek'],
            'tbkategori_id' => 1,
            'jumlah' => $request['jumlah'],

        ]);
        return redirect('/Jenis');
    }
    public function index()
    {
        $jenis = DB::table('tbjenis')->get();
        return view('jenis.tampil', ['jenis' => $jenis]);
    }

    public function edit($id)
    {
        $jenis = DB::table('tbjenis')->find($id);
        return view('jenis.edit', ['jenis' => $jenis]);
    }
}
