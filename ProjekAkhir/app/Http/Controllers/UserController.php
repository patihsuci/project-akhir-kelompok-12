<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function create()
    {
        return view('user.tambah');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'password' => 'required',
      
        ]);
    }
    public function index()
    {
        $user = DB::table('tbuser')->get();
        return view('user.tampil', ['user' => $user]);
    }

    public function edit($id)
    {
        $user = DB::table('tbuser')->find($id);
        return view('user.edit', ['user' => $user]);
    }

}
