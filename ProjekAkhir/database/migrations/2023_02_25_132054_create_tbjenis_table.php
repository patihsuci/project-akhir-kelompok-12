<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbjenisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbjenis', function (Blueprint $table) {
            $table->id();            $table->string('gambar');
            $table->string('merek');
            $table->integer('jumlah');
            $table->unsignedBigInteger('tbkategori_id');
            $table->foreign('tbkategori_id')->references('id')->on('tbkategori') ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbjenis');
    }
}

